import { initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyCbaWfgk_eU0QOWcLGFtbdeJY1GGl9gVic",
    authDomain: "chat-application-1b6a0.firebaseapp.com",
    projectId: "chat-application-1b6a0",
    storageBucket: "chat-application-1b6a0.appspot.com",
    messagingSenderId: "419877841588",
    appId: "1:419877841588:web:dd46f233094aa1518305e1"
};

export const app = initializeApp(firebaseConfig);


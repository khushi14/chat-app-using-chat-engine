
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import {AuthProvider} from './context/AuthContext'

// components
import Login from './components/Login';
import Chat from './components/Chat';

import { app } from './firebase';

function App() {
  return (
    <div style={{fontFamily: 'Avenir'}}>
       <Router>
         <AuthProvider>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/chats" element={<Chat />} />
            </Routes>
           
         </AuthProvider>
           
       </Router>
    </div>
  );
}

export default App;

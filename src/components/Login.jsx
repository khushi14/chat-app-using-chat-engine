import React from 'react'
import {GoogleOutlined, FacebookFilled} from '@ant-design/icons'
import { getAuth, signInWithRedirect, GoogleAuthProvider, FacebookAuthProvider } from "firebase/auth";


const Login = () => {
  const handleGoogleAction = ()=>{
    const auth = getAuth();
    const provider = new GoogleAuthProvider();
    signInWithRedirect(auth, provider)
  }

  const handleFacebookAction = ()=>{
    const auth = getAuth();
    const provider = new FacebookAuthProvider();
    signInWithRedirect(auth, provider)
  }

  return (
    <div id="login-page">
        <div id="login-card">
            <h2 style={{color: 'orange'}}>WELCOME TO CHAT APP</h2>
            <div className="login-button google" onClick={handleGoogleAction}>
                <GoogleOutlined /> Sign in with Google
            </div>
            <br /> <br />
            <div className="login-button facebook" onClick={handleFacebookAction}>
            <FacebookFilled /> Sign in with Facebook
            </div>
        </div>
    </div>
  )
}

export default Login
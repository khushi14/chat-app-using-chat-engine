import React, {useRef, useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom'
import { ChatEngine } from 'react-chat-engine'
import {getAuth, signOut } from 'firebase/auth'

// context file
import {useAuth} from '../context/AuthContext'
import axios from 'axios'

const Chat = () => {
  const auth = getAuth();
  const navigate = useNavigate();

  const [loading,setLoading]  = useState(true)

  const {user} = useAuth();
  console.log(user);

  const handleLogout = async ()=>{
      await signOut(auth);
      navigate('/');
  }

  async function getFile(url) {
    let response = await fetch(url);
    let data = await response.blob();  // in the binary format
    return new File([data], "userPhoto.jpg", { type: 'image/jpeg' });
  }


  useEffect( ()=> {
    if(!user){
       navigate('/');
       return;
    }

    axios.get('https://api.chatengine.io/users/me/', {
      headers: { 
        "project-id": process.env.REACT_APP_CHAT_ENGINE_PROJECTID,
        "user-name": user.email,
        "user-secret": user.uid
      }
    })
    .then( ()=> setLoading(false) )
    .catch( (e)=> {

      let formData = new FormData()
      formData.append('email', user.email)
      formData.append('username', user.email)
      formData.append('secret', user.uid)

      getFile(user.photoURL)
      .then((avatar) => {
         formData.append('avatar', avatar, avatar.name)
          axios.post('https://api.chatengine.io/users', formData, {
            headers: { 
              "private-key": process.env.REACT_APP_CHAT_ENGINE_KEY
            }
          })
          .then(() => setLoading(false))
          .catch((e) => console.log('e', e.response))
      })

    })

  }, [user, navigate])

  if (!user || loading) return "Loading..."

  return (

    <div className="chats-page">
       <div className="nav-bar">
         <div className="logo-tab">Chat App</div>
         <div className="logout-tab" onClick={handleLogout}>Logout</div>
       </div>

       <ChatEngine 
         height='calc(100vh - 66px)'
         projectID={process.env.REACT_APP_CHAT_ENGINE_PROJECTID}
         userName={user.email}
         userSecret={user.uid}
       />
    </div>
  )
}

export default Chat
import  React, {useState, useEffect, useContext, createContext} from 'react';
import {useNavigate} from 'react-router-dom'
import { getAuth } from "firebase/auth";

const AuthContext = createContext();
export const useAuth =  () => useContext(AuthContext);

export const AuthProvider = ({children}) => {
    const auth = getAuth();
    const [loading, setLoading] =useState(true);
    const [user, setUser] = useState(null); 

    const navigate = useNavigate()

    useEffect( ()=> {
       auth.onAuthStateChanged( (user) => {
           setUser(user);
           setLoading(false);
           if(user){
            navigate('/chats')
           }
       })
    }, [user, navigate, auth])

    const value = {user};

    return (
        <AuthContext.Provider value={value}>
             {!loading && children}
        </AuthContext.Provider>
    )

}
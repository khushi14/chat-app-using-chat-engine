# Chat App

Created a firebase Chat Application With social authentication including Google and Facebook using Firebase.

This app is created using [Chat Engine](https://chatengine.io/)

Chat Engine helps to build different features like: 
image support, sound notifications, ability to created multiple rooms, online statuses, etc.

### Deployed Link

https://chat-app-using-chat-engine.netlify.app/
